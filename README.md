# vim-tabby

vim tab movements made intuitive, repeatable with `count`, and toroidal
movements.

<!-- Insert illustrative GIF here -->

---

## Installation

Install `vim-tabby` with your favorite plugin manager.
Below are the instructions for the built-in package manager:

```
# vim
git clone https://gitlab.com/japorized/vim-tabby.git $HOME/.vim/pack/utilities/start/vim-tabby

# neovim
git clone https://gitlab.com/japorized/vim-tabby.git $HOME/.config/nvim/pack/utilities/start/vim-tabby
```

No default keybinds are provided OOTB. The following `<Plug>` maps are provided.
```vimscript
nnoremap <silent> <Plug>(MoveTabToRight) :<c-u>execute v:count1 . 'MoveTabToRight'<CR>
nnoremap <silent> <Plug>(MoveTabToLeft) :<c-u>execute v:count1 . 'MoveTabToLeft'<CR>
nnoremap <silent> <Plug>(MoveTabToEnd) :tabmove $<CR>
nnoremap <silent> <Plug>(MoveTabToStart) :tabmove 0<CR>
```

You can make your own mappings like so:
```vimscript
nnoremap <silent> mtn <Plug>(MoveTabToRight)
nnoremap <silent> mtp <Plug>(MoveTabToLeft)
nnoremap <silent> mt] <Plug>(MoveTabToEnd)
nnoremap <silent> mt[ <Plug>(MoveTabToStart)
```
---

## Usage

Suppose you've set up the keybinds as in the end of last section.

- To move a tab 2 positions backward, hit `2mtp`.
- To move a tab 4 positions forward, hit `4mtn`.

<!-- vim:tw=80
-->
