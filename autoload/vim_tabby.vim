" Big NOTE: read :h tabmove

function! vim_tabby#MoveTabToRight(count) abort
  let num_of_tabs = tabpagenr("$")
  let current_tab_number = tabpagenr()
  " A bit of careful thinking is needed here to know why we employ this
  " formula.
  " Example 1
  " Curent tab: 3, total tabs: 4, want to move right by 1
  " Desired result, tab moves to pos 4
  " Example 2
  " Curent tab: 4, total tabs: 4, want to move right by 1
  " Desired result, tab moves to pos 0
  " Example 3
  " Curent tab: 3, total tabs: 4, want to move right by 2
  " Desired result, tab moves to pos 0
  " Example 3
  " Curent tab: 4, total tabs: 4, want to move right by 2
  " Desired result, tab moves to pos 1
  let new_tab_number = (current_tab_number + a:count) % (num_of_tabs + 1)

  execute "tabmove " .. new_tab_number
endfunction

function! vim_tabby#MoveTabToLeft(count) abort
  let num_of_tabs = tabpagenr("$")
  let current_tab_number = tabpagenr()
  let new_tab_number = vim_tabby#PositiveModulo((current_tab_number - a:count) % num_of_tabs, num_of_tabs)
  " echom new_tab_number

  if new_tab_number == 0
    execute "tabmove $"
  elseif new_tab_number == 1
    execute "tabmove 0"
  else
    execute "tabmove " .. (new_tab_number - 1)
  endif
endfunction

function! vim_tabby#PositiveModulo(number, modulo) abort
  if a:number < 0
    return (a:number % a:modulo) + a:modulo
  else
    return a:number % a:modulo
  endif
endfunction
