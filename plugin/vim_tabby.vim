command! -nargs=0 -count=1 MoveTabToRight call vim_tabby#MoveTabToRight(<count>)
command! -nargs=0 -count=1 MoveTabToLeft call vim_tabby#MoveTabToLeft(<count>)

nnoremap <silent> <Plug>(MoveTabToRight) :<c-u>execute v:count1 . 'MoveTabToRight'<CR>
nnoremap <silent> <Plug>(MoveTabToLeft) :<c-u>execute v:count1 . 'MoveTabToLeft'<CR>
nnoremap <silent> <Plug>(MoveTabToEnd) :tabmove $<CR>
nnoremap <silent> <Plug>(MoveTabToStart) :tabmove 0<CR>
